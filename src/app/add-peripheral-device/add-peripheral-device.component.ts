import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpResponse} from '@angular/common/http';
import {PeripheralDeviceService} from '../services/peripheral-device.service';

@Component({
  selector: 'app-add-peripheral-device',
  templateUrl: './add-peripheral-device.component.html',
  styleUrls: ['./add-peripheral-device.component.css']
})
export class AddPeripheralDeviceComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private peripheralDeviceService: PeripheralDeviceService,
              private activatedRoute: ActivatedRoute) {
  }

  addForm: FormGroup;
  idGateway = -1;

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: any) => {
      if (params['idGateway']) {
        this.idGateway = params['idGateway'];

        this.addForm = this.formBuilder.group({
          vendor: ['', Validators.required]
        });
      }
    });
  }

  onSubmit() {
    if (this.addForm.valid) {
      this.peripheralDeviceService.addPeripheralDevice(this.idGateway, this.addForm.value).subscribe((data: HttpResponse<any>) => {
        if (data.status === 201) {
          this.router.navigate(['list-gateway']);
        }
      }, error => {
        if (error.status === 404) {
          alert('The gateway with id (' + this.idGateway + ') wasn\'t found in the database');
        } else if (error.status === 509) {
          alert('The gateway has been reached the maximum number of peripheral devices allowed');
        } else {
          alert('An error was found');
        }
      });
    } else {
      let textError = '';
      if (this.addForm.controls.vendor.errors != null) {
        textError += '- The value in the field "vendor" is not valid';
      }
      alert(textError);
    }
  }

  goBack() {
    this.router.navigate(['list-gateway']);
  }

}
