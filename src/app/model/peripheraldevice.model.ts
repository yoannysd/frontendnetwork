export class PeripheralDevice {
  date: string;
  status: number;
  uid: number;
  vendor: string;
}
