import { PeripheralDevice } from './peripheraldevice.model';
export class Gateway {

  id: number;
  serialNumber: string;
  ipv4: string;
  name: string;
  peripheralDevices: PeripheralDevice[];
}
