import { TestBed } from '@angular/core/testing';

import { PeripheralDeviceService } from './peripheral-device.service';

describe('PeripheralDeviceServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PeripheralDeviceService = TestBed.get(PeripheralDeviceService);
    expect(service).toBeTruthy();
  });
});
