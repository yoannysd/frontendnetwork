import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {ApiService} from '../core/api.service';
import {PeripheralDevice} from '../model/peripheraldevice.model';

@Injectable({
  providedIn: 'root'
})
export class PeripheralDeviceService extends ApiService {

  private urlPeripheral = '/peripheral';

  constructor(http: HttpClient) {
    super(http);
  }

  addPeripheralDevice(idGateway: number, peripheralDevice: PeripheralDevice): Observable<HttpResponse<any>> {
    return this.post<any>(this.urlPeripheral + '/' + idGateway, peripheralDevice);
  }

  deletePeripheralDevice(uidPeripheralDevice: number): Observable<HttpResponse<any>> {
    return this.delete<any>(this.urlPeripheral, uidPeripheralDevice);
  }

}
