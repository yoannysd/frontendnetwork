import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {ApiService} from '../core/api.service';
import {Gateway} from '../model/gateway.model';

@Injectable({
  providedIn: 'root'
})
export class GatewayService extends ApiService {

  private urlGateway = '/gateway';

  constructor(http: HttpClient) {
    super(http);
  }

  getGateways(): Observable<HttpResponse<Gateway[]>> {
    return this.get<Gateway[]>(this.urlGateway);
  }

  addGateway(gateway: Gateway): Observable<HttpResponse<any>> {
    return this.post<any>(this.urlGateway, gateway);
  }

  deleteGateway(idGateway: number): Observable<HttpResponse<any>> {
    return this.delete<any>(this.urlGateway, idGateway);
  }

}
