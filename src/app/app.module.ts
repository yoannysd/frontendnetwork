import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {DataTablesModule} from 'angular-datatables';
import {MaterialsModule} from './modules/materials/materials.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {ApiService} from './core/api.service';

import {AddgatewayComponent} from './addgateway/addgateway.component';
import {ListgatewayComponent} from './listgateway/listgateway.component';
import {GatewayService} from './services/gateway.service';
import {AddPeripheralDeviceComponent} from './add-peripheral-device/add-peripheral-device.component';

@NgModule({
  declarations: [
    AppComponent,
    AddgatewayComponent,
    ListgatewayComponent,
    AddPeripheralDeviceComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    DataTablesModule,
    ReactiveFormsModule,

    BrowserAnimationsModule,
    MaterialsModule,
    AppRoutingModule
  ],
  providers: [
    ApiService,
    GatewayService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
