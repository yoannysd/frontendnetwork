import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class ApiService {

  protected baseUrl = 'http://localhost:3001/network/api';

  constructor(protected http: HttpClient) {
  }

  protected get<T>(specificUrl: string): Observable<HttpResponse<T>> {
    return this.http.get<T>(this.baseUrl + specificUrl, {
      observe: 'response',
      responseType: 'json'
    });
  }

  protected post<T>(specificUrl: string, body: any): Observable<HttpResponse<T>> {
    return this.http.post<T>(this.baseUrl + specificUrl, body, {
      observe: 'response',
      responseType: 'json'
    });
  }

  protected delete<T>(specificUrl: string, id: number): Observable<HttpResponse<T>> {
    return this.http.delete<T>(this.baseUrl + specificUrl + '/' + id, {
      observe: 'response',
      responseType: 'json'
    });
  }

}
