import {Component, OnInit} from '@angular/core';
import {GatewayService} from '../services/gateway.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Gateway} from '../model/gateway.model';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-addgateway',
  templateUrl: './addgateway.component.html',
  styleUrls: ['./addgateway.component.css']
})
export class AddgatewayComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router: Router, private gatewayService: GatewayService) {
  }

  addForm: FormGroup;

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      serialNumber: ['', Validators.required],
      name: ['', Validators.required],
      ipv4: ['', [Validators.required,
        Validators.pattern('^(([1-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.)' +
          '(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){2}' +
          '([1-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$')]]
    });
  }

  onSubmit() {
    if (this.addForm.valid) {
      this.gatewayService.addGateway(this.addForm.value).subscribe((data: HttpResponse<any>) => {
        if (data.status === 201) {
          this.router.navigate(['list-gateway']);
        }
      }, error => {
        if (error.status === 409) {
          const g: Gateway = this.addForm.value;
          alert('The gateway with serial number (' + g.serialNumber + ') already exists');
        } else {
          alert('An error was found');
        }
      });
    } else {
      let textError = '';
      if (this.addForm.controls.serialNumber.errors != null) {
        textError += '- The value in the field "serialNumber" is not valid';
      }
      if (this.addForm.controls.name.errors != null) {
        textError += (textError === '') ? '- The value in the field "name" is not valid' : '\n- The value in the field "name" is not valid';
      }
      if (this.addForm.controls.ipv4.errors != null) {
        textError += (textError === '') ? '- The value in the field "ipv4" is not valid' : '\n- The value in the field "ipv4" is not valid';
      }
      alert(textError);
    }
  }

  goBack() {
    this.router.navigate(['list-gateway']);
  }
}
