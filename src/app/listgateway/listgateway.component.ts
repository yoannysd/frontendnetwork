import {Component, OnInit, ElementRef} from '@angular/core';
import {GatewayService} from '../services/gateway.service';
import {Gateway} from '../model/gateway.model';
import {Router} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {PeripheralDeviceService} from '../services/peripheral-device.service';

@Component({
  selector: 'app-listgateway',
  templateUrl: './listgateway.component.html',
  styleUrls: ['./listgateway.component.css']
})
export class ListgatewayComponent implements OnInit {

  public gateways: Gateway[];
  public currentGatewayId = -1;

  constructor(private gatewayService: GatewayService,
              private peripheralDeviceService: PeripheralDeviceService,
              private router: Router,
              private elementReference: ElementRef) {
  }

  ngOnInit() {
    this.getGateways();
  }

  deleteGateway(idGateway) {
    this.gatewayService.deleteGateway(idGateway).subscribe((data: HttpResponse<any>) => {
      if (data.status === 200) {
        if (this.currentGatewayId !== -1) {
          this._cleanPreviousDetails();
          this.currentGatewayId = -1;
        }
        this.getGateways();
      }
    }, error => {
      if (error.status === 404) {
        alert('The gateway with id (' + idGateway + ') wasn\'t found in the database');
      } else {
        alert('An error was found');
      }
    });
  }

  deletePeripheralDevice(uidPeripheralDevice: number) {
    this.peripheralDeviceService.deletePeripheralDevice(uidPeripheralDevice).subscribe((data: HttpResponse<any>) => {
      if (data.status === 200) {
        if (this.currentGatewayId !== -1) {
          this._cleanPreviousDetails();
          this.currentGatewayId = -1;
        }
        this.getGateways();
      }
    }, error => {
      if (error.status === 404) {
        alert('The peripheral device with uid (' + uidPeripheralDevice + ') wasn\'t found in the database');
      } else {
        alert('An error was found');
      }
    });
  }

  detailsGateway(gateway: Gateway) {
    const referenceRow = document.getElementById('row-gateway' + gateway.id);

    if (this.currentGatewayId !== gateway.id) {
      if (this.currentGatewayId !== -1) {
        this._cleanPreviousDetails();
      }
      this.currentGatewayId = gateway.id;

      const rowTitle = document.createElement('tr');
      rowTitle.className = 'details';
      rowTitle.innerHTML = '<td><strong>UID</strong></td>' +
        '<td><strong>VENDOR</strong></td>' +
        '<td><strong>STATUS</strong></td>' +
        '<td><strong>DATE</strong></td>' +
        '<td><strong>ACTIONS</strong></td>';
      referenceRow.parentNode.insertBefore(rowTitle, referenceRow.nextSibling);

      for (const pd of gateway.peripheralDevices) {

        const nextRow = document.createElement('tr');
        nextRow.innerHTML = '<td>' + pd.uid + '</td><td>' + pd.vendor + '</td><td>' + pd.status + '</td><td>' + pd.date + '</td>';
        const action = document.createElement('td');
        const buttonDelete = document.createElement('button');
        buttonDelete.id = 'deleteBtn' + pd.uid;
        buttonDelete.innerText = 'Delete';
        buttonDelete.className = 'btn btn-danger';
        buttonDelete.addEventListener('click',
          this.deletePeripheralDevice.bind(this, [pd.uid]));
        action.appendChild(buttonDelete);
        nextRow.appendChild(action);
        nextRow.className = 'details';

        rowTitle.parentNode.insertBefore(nextRow, rowTitle.nextSibling);
      }
    } else {
      // removing detail's rows
      let countP = gateway.peripheralDevices.length;
      if (countP > 0) {
        countP++; // In order to remove also the row that shows the name of columns for peripherical devices.
        while (countP > 0) {
          let nextDetailRow = referenceRow.nextSibling;

          while (nextDetailRow && nextDetailRow.nodeType !== 1) {
            nextDetailRow = nextDetailRow.nextSibling;
          }

          if (nextDetailRow && nextDetailRow.nodeType === 1) {
            nextDetailRow.parentNode.removeChild(nextDetailRow);
          }
          countP--;
        }
      }
      this.currentGatewayId = -1;
    }
  }

  getGateways() {
    this.gatewayService.getGateways().subscribe((data: HttpResponse<Gateway[]>) => {
      if (data.status === 200) {
        this.gateways = data.body;
      }
    });
  }

  goAddGatewayPage() {
    this.router.navigate(['add-gateway']);
  }

  goAddPeripheralDevicePage(gateway: Gateway) {
    if (gateway.peripheralDevices.length <= 9) {
      this.router.navigate(['add-peripheral-device', gateway.id]);
    } else {
      alert('The gateway has been reached the maximum number of peripheral devices allowed');
    }
  }

  private _cleanPreviousDetails() {
    const rows = document.getElementsByClassName('details');
    Array.from(rows).forEach(e => {
      const parent = e.parentNode;
      parent.removeChild(e);
    });
  }

}
