import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AddgatewayComponent} from './addgateway/addgateway.component';
import {ListgatewayComponent} from './listgateway/listgateway.component';
import {AddPeripheralDeviceComponent} from './add-peripheral-device/add-peripheral-device.component';

const routes: Routes = [
  {path: 'list-gateway', component: ListgatewayComponent},
  {path: 'add-gateway', component: AddgatewayComponent},
  {path: 'add-peripheral-device/:idGateway', component: AddPeripheralDeviceComponent},
  {path: '', component: ListgatewayComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
