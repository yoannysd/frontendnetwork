# FrontendNetwork

Frontend of the application

YOU CAN DOWNLOAD THE COMPILED FRONTEND USING THIS URL: https://gitlab.com/api/v4/projects/21714369/jobs/artifacts/dev/download?job=build

====>Steps to CONNECT the FRONTEND to the BACKEND:<====

1- It is necessary before to know by which port is running the api of the backend(See the README.md file of the BackendNetwork). By default the frontend is listen to the port 3001, this is the port in which the backend is running by default too.

2- After know this, you has to download the compiled generated in the following url https://gitlab.com/api/v4/projects/21714369/jobs/artifacts/dev/download?job=build

3- Copy the compressed folder (network-frontend.zip) to the public directory of an application server, for example: Apache. Extract it there and configure a VirtualHost for it. For example:
<VirtualHost *:84>
	ServerName localhost
	DocumentRoot c:/wamp64/www/dist/app-angular
	<Directory c:/wamp64/www/dist/app-angular>
		RewriteEngine on
		# Don't rewrite files or directories
		RewriteCond %{REQUEST_FILENAME} -f [OR]
		RewriteCond %{REQUEST_FILENAME} -d
		RewriteRule ^ - [L]
		# Rewrite everything else to index.html
		# to allow html5 state links
	    RewriteRule ^ index.html [L]
    </Directory>
</VirtualHost>